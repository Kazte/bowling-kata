﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowlingController : MonoBehaviour
{
    [SerializeField]
    private Bowling bowling;

    [SerializeField]
    private List<FrameUI> frames;

    void Awake()
    {
        bowling = new Bowling();
    }

    public void Throw(int value)
    {
        frames[bowling.CurrentFrameIndex].setThrowText(bowling.GetCurrentFrameRoll(), value.ToString());

        bowling.Throw(value);

        // frames[bowling.CurrentFrameIndex].setResultText(bowling.GetTurnScore(bowling.CurrentFrameIndex + 1).ToString());
    }
}