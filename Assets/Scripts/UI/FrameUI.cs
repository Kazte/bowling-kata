﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FrameUI : MonoBehaviour
{
    //[SerializeField]
    //private Text FirstThrow;
    //[SerializeField]
    //private Text SecondThrow;
    [SerializeField]
    private Text Result;
    [SerializeField]
    private List<Text> scoreTexts;

    public void setThrowText(int index, string text)
    {
        scoreTexts[index].text = text;
    }

    public void setResultText(string text)
    {
        Result.text = text;
    }
}
