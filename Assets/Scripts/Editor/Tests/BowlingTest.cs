﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class BowlingTest
    {
        private Bowling bowling;

        [SetUp]
        public void SetUp()
        {
            bowling = new Bowling();
        }

        [Test]
        public void One_Turn_Under_Max_Pines_Should_Sum_Fallen_Pines_Count()
        {
            bowling.Throw(3);
            bowling.Throw(2);

            Assert.AreEqual(5, bowling.CurrentTurnScore());
        }

        [Test]
        public void One_Turn_Two_Throw_All_Pines_Down_Is_Spare()
        {
            bowling.Throw(5);
            bowling.Throw(5);

            Assert.True(bowling.IsSpare);
        }

        [Test]
        public void One_Turn_One_Throw_All_Pines_Down_Is_Strike()
        {
            bowling.Throw(10);

            Assert.True(bowling.IsStrike);
        }

        [Test]
        public void Turn_After_Spare_Gives_Extra_Points_Only_First_Throw()
        {
            bowling.Throw(5);
            bowling.Throw(5);

            bowling.Throw(2);
            bowling.Throw(3);

            Assert.AreEqual(12, bowling.GetTurnScore(1));
        }

        [Test]
        public void Turn_After_Strike_Gives_Extra_Points_Both_Throws()
        {
            bowling.Throw(10);

            bowling.Throw(2);
            bowling.Throw(3);

            Assert.AreEqual(15, bowling.GetTurnScore(1));
        }

        [Test]
        public void Two_Turns_After_Spare_Doesnt_Give_Extra_Points()
        {
            bowling.Throw(5);
            bowling.Throw(5);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);


            Assert.AreEqual(22, bowling.GetTotalScore());
        }

        [Test]
        public void Two_Turns_After_Strike_Doesnt_Give_Extra_Points()
        {
            bowling.Throw(10);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);


            Assert.AreEqual(25, bowling.GetTotalScore());
        }

        [Test]
        public void Turn_After_Strike_And_Spare()
        {
            bowling.Throw(10);

            bowling.Throw(5);
            bowling.Throw(5);

            bowling.Throw(2);
            bowling.Throw(3);

            Assert.AreEqual(37, bowling.GetTotalScore());
        }

        [Test]
        public void All_Turns_Without_Bonification_Throw()
        {
            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            Assert.AreEqual(50, bowling.GetTotalScore());
        }

        [Test]
        public void All_Turns_With_Spare_At_Last_Turn()
        {
            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(5);
            bowling.Throw(5);
            bowling.Throw(5);

            Assert.AreEqual(60, bowling.GetTotalScore());
        }

        [Test]
        public void All_Turns_With_Strike_At_Last_Turn()
        {
            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(10);
            bowling.Throw(3);
            bowling.Throw(5);

            Assert.AreEqual(63, bowling.GetTotalScore());
        }

        [Test]
        public void All_Turns_With_Double_Strike_At_Last_Turn()
        {
            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(10);
            bowling.Throw(10);
            bowling.Throw(9);

            Assert.AreEqual(74, bowling.GetTotalScore());
        }

        [Test]
        public void All_Turns_Without_Bonus_At_Last_Turn()
        {
            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(2);
            bowling.Throw(3);

            bowling.Throw(4);
            bowling.Throw(3);

            Assert.AreEqual(52, bowling.GetTotalScore());
            Assert.IsTrue(bowling.IsMatchOver());
        }
    }
}