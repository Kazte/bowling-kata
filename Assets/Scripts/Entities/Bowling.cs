﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;

public class BowlingManager
{
}

[Serializable]
public class Bowling
{
    private const int MAX_TURNS = 10;
    private const int MAX_THROWS = 2;
    private const int MAX_PINES = 10;
    private const int EXTRA_POINTS = 10;
    private int currentPinesAmount;


    public Bowling()
    {
        currentPinesAmount = MAX_PINES;

        frames = new List<Frame>();

        for (var i = 0; i < MAX_TURNS - 1; i++)
        {
            frames.Add(new Frame(2));
        }

        frames.Add(new Frame(3));
    }

    private List<Frame> frames;
    private int currentFrameIndex = 0;

    public bool IsStrike => frames[currentFrameIndex - 1].IsStrike;
    public bool IsSpare => frames[currentFrameIndex - 1].IsSpare;

    public int CurrentFrameIndex => currentFrameIndex;

    public void Throw(int pines)
    {
        currentPinesAmount -= pines;

        frames[currentFrameIndex].SetRollScore(pines);

        if (currentFrameIndex > 0)
        {
            if (frames[currentFrameIndex - 1].IsStrike)
            {
                frames[currentFrameIndex - 1].AddBonusScore(frames[currentFrameIndex].GetScore());
            }
            else if (frames[currentFrameIndex - 1].IsSpare)
            {
                frames[currentFrameIndex - 1].AddBonusScore(frames[currentFrameIndex].GetRollScore(0));
            }
        }

        if (frames[currentFrameIndex].IsFull)
        {
            currentFrameIndex++;
            currentPinesAmount = MAX_PINES;
        }
    }

    public int CurrentTurnScore()
    {
        return frames[currentFrameIndex - 1].GetScore();
    }

    public int GetTurnScore(int turn)
    {
        return frames[turn - 1].GetScore();
    }

    public int GetTotalScore()
    {
        var sum = 0;

        foreach (var frame in frames)
        {
            sum += frame.GetScore();
        }

        return sum;
    }

    public int GetPartialScore(int endFrame)
    {
        var sum = 0;

        for (int i = 0; i < endFrame + 1; i++)
        {
            sum += frames[i].GetScore();
        }

        return sum;
    }

    public int PinesLeft() => currentPinesAmount;

    public int GetCurrentFrameRoll() => frames[currentFrameIndex].GetCurrentRoll();

    public bool IsMatchOver()
    {
        return frames.All(x => x.IsFull);
    }
}