﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Frame
{
    private List<int> rollsScore;

    private bool isSpare;

    private bool isStrike;

    private bool isFull;

    private int frameBonus;

    public bool IsSpare => isSpare;

    public bool IsStrike => isStrike;

    public bool IsFull => isFull;


    public Frame(int rollsAmount)
    {
        rollsScore = new List<int>();

        for (var i = 0; i < rollsAmount; i++)
        {
            rollsScore.Add(-1);
        }
    }

    public void SetRollScore(int score)
    {
        var index = GetCurrentRoll();

        if (index != -1)
        {
            rollsScore[index] = score;
        }

        VerifyStrikeOrSpare(score, index);
        VerifyIsFull();
    }

    private void VerifyIsFull()
    {
        if (rollsScore.IndexOf(-1) == -1 ||
            (isStrike && rollsScore.Count != 3) ||
            (rollsScore.Count == 3 && !isStrike && !isSpare && rollsScore.Count(x => x == -1) == 1))
        {
            isFull = true;
        }
    }

    private void VerifyStrikeOrSpare(int score, int index)
    {
        if (index == 0 && score == 10)
        {
            isStrike = true;
        }
        else if (index == 1 && GetScore() == 10)
        {
            isSpare = true;
        }
    }

    public int GetScore()
    {
        var sum = 0;

        foreach (var rollScore in rollsScore)
        {
            if (rollScore != -1)
            {
                sum += rollScore;
            }
        }

        return sum + frameBonus;
    }

    public void AddBonusScore(int bonusScore)
    {
        frameBonus = bonusScore;
    }

    public int GetRollScore(int rollIndex) => rollsScore[rollIndex];

    public int GetCurrentRoll() => rollsScore.IndexOf(-1);
}