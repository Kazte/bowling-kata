﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    [SerializeField]
    private Bowling bowling;

    private void Awake()
    {
        bowling = new Bowling();
    }

    private void Start()
    {
        bowling.Throw(10);

        bowling.Throw(5);
        bowling.Throw(5);

        bowling.Throw(2);
        bowling.Throw(3);

        Debug.Log(bowling.GetTotalScore());
    }
}